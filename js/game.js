(function ($) {
  var left = 37,
      up = 38,
      right = 39,
      down = 40;

  var Tile = function (number) {
    this.number = number || 0;
    this.merged = false;
  };

  Tile.prototype.isEmpty = function () {
    return this.number == 0;
  };
  Tile.prototype.setMerged = function (merged) {
    this.merged = merged;
  };

  var Game = {
    gameState: [],
    score: 0,
    init: function (boardSize) {
      var that = this;
      this.initializeGameState(boardSize);
      $(document).keyup(function (event) {
        that.updateGameState(event.keyCode);
      });
      $('#reset_game').click(function () {
        that.initializeGameState(boardSize);
        that.tryIntroduceNewTile(left);
        that.printGameState();
      });
      this.tryIntroduceNewTile(left);
      return this;
    },
    initializeGameState: function (boardSize) {
      this.gameState = [];
      for (var i = 0; i < boardSize; i++) {
        var row = [];
        for (var j = 0; j < boardSize; j++) {
          row.push(new Tile(0));
        }
        this.gameState.push(row);
      }
      this.score = 0;
    },
    printGameState: function () {
      $('.game').empty();
      for (var i = 0; i < this.gameState.length; i++) {
        var row = this.gameState[i];
        for (var j = 0; j < row.length; j++) {
          var tileNumber = row[j].number;
          var divTile = $('<div class="tile tile-' + tileNumber + '"></div>').text(tileNumber ? tileNumber : '');
          $('.game').append(divTile);
          $('#scoreText').text(this.score);
        }
      }
    },
    canMerge: function (tileA, tileB) {
      return ( tileA != undefined && tileB != undefined && !tileA.merged && !tileA.merged) && ((tileA.isEmpty() && tileB.isEmpty()) || (tileA.number == tileB.number));
    },
    mergeTiles: function (tileA, tileB) {
      if (tileA.number == tileB.number) {
        var newTileNumber = tileA.number + tileB.number;
        var newTile = new Tile(newTileNumber);
        newTile.setMerged(true);
        this.score = this.score + newTile.number;
        return newTile;
      } else if (tileA.isEmpty()) {
        tileB.setMerged(false);
        return tileB;
      }
    },
    updateGameState: function (direction) {
      var curPos;
      if (direction == left) {
        console.log('Left Pressed');
        for (curPos = 0; curPos < this.gameState.length; curPos++) {
          this.flatten(false, this.gameState[curPos]);
        }
      } else if (direction == right) {
        console.log('Right Pressed');
        for (curPos = 0; curPos < this.gameState.length; curPos++) {
          this.flatten(true, this.gameState[curPos]);
        }
      } else if (direction == up) {
        console.log('Up Pressed');
        for (curPos = 0; curPos < this.gameState.length; curPos++) {
          this.setRowAsColumn(curPos, this.flatten(false, this.getColumnAsRow(curPos)));
        }
      } else if (direction == down) {
        console.log('Down Pressed');
        for (curPos = 0; curPos < this.gameState.length; curPos++) {
          this.setRowAsColumn(curPos, this.flatten(true, this.getColumnAsRow(curPos)));
        }
      }
      this.tryIntroduceNewTile(direction);
      this.printGameState();
    },
    getColumnAsRow: function (columnIndex) {
      var row = [];
      for (var curRow = 0; curRow < this.gameState.length; curRow++)
        row.push(this.gameState[curRow][columnIndex]);
      console.log(JSON.stringify(row));
      return row;
    },
    setRowAsColumn: function (columnIndex, row) {
      for (var curRow = 0; curRow < this.gameState.length; curRow++)
        this.gameState[curRow][columnIndex] = row[curRow];
      console.log(JSON.stringify(this.gameState))
    },
    tryMerge: function (row, i, additive) {
      if (this.canMerge(row[i + additive], row[i])) {
        row[i + additive] = this.mergeTiles(row[i + additive], row[i]);
        row[i] = new Tile(0);
        return true;
      }
      return false;
    },
    flatten: function (forward, row) {
      var curPos, nextPos, lastTile, cellWasEmpty;
      console.log('Before:' + JSON.stringify(row));
      if (!forward) {
        for (curPos = 0; curPos < row.length;) {
          nextPos = curPos;
          cellWasEmpty = row[curPos].isEmpty();
          do {
            lastTile = row[nextPos];
            nextPos++;
          } while (lastTile.isEmpty() && nextPos < row.length);
          row[curPos] = lastTile;
          if (cellWasEmpty)
            row[nextPos - 1] = new Tile();
          if (!this.tryMerge(row, curPos, (-1)))
            curPos++;
        }
      }
      if (forward) {
        for (curPos = row.length - 1; curPos >= 0;) {
          nextPos = curPos;
          cellWasEmpty = row[curPos].isEmpty();
          do {
            lastTile = row[nextPos];
            nextPos--;
          } while (lastTile.isEmpty() && nextPos >= 0);
          row[curPos] = lastTile;
          if (cellWasEmpty)
            row[nextPos + 1] = new Tile();
          if (!this.tryMerge(row, curPos, 1)) {
            curPos--;
          }
        }
      }
      //set merge to false
      for (curPos = 0; curPos < row.length; curPos++) row[curPos].setMerged(false);
      console.log('After:' + JSON.stringify(row));
      return row;
    },
    tryIntroduceNewTile: function (direction) {
      var newTile = new Tile(this.getRandomTileNo());
      var newPosition = this.getNextAvailablePosition(direction);
      if (newPosition) this.gameState[newPosition.row][newPosition.col] = newTile;
    },
    getRandomTileNo: function () {
      return (Math.round(Math.random() * (1))) ? 2 : 4;
    },
    getNextAvailablePosition: function (direction) {
      var positionRoW;
      var positionColumn;
      var tile;
      var resultPosition;
      if (direction == left) {
        positionRoW = 0;
        positionColumn = this.gameState.length - 1;
        do {
          tile = this.gameState[positionRoW++][positionColumn];
        } while (!tile.isEmpty() && positionRoW < this.gameState.length);
        if (tile.isEmpty()) resultPosition = {row: positionRoW - 1, col: positionColumn};
      } else if (direction == right) {
        positionRoW = 0;
        positionColumn = 0;
        do {
          tile = this.gameState[positionRoW++][positionColumn];
        } while (!tile.isEmpty() && positionRoW < this.gameState.length);
        if (tile.isEmpty()) resultPosition = {row: positionRoW - 1, col: positionColumn};
      } else if (direction == up) {
        positionRoW = this.gameState.length - 1;
        positionColumn = 0;
        do {
          tile = this.gameState[positionRoW][positionColumn++];
        } while (!tile.isEmpty() && positionColumn < this.gameState.length);
        if (tile.isEmpty()) resultPosition = {row: positionRoW, col: positionColumn - 1};
      } else if (direction == down) {
        positionRoW = 0;
        positionColumn = 0;
        do {
          tile = this.gameState[positionRoW][positionColumn++];
        } while (!tile.isEmpty() && positionColumn < this.gameState.length);
        if (tile.isEmpty()) resultPosition = {row: positionRoW, col: positionColumn - 1};
      }
      return resultPosition;
    }
  };

  $(document).ready(function () {
    var game = Game.init(4);
    game.printGameState();
  });
})(jQuery);